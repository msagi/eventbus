package com.msagi.eventbus.events;

import com.msagi.eventbus.EventBus;

/**
 * Test Event that is used in the JUnit tests
 * @author yanislav.mihaylov
 */
public class TestEvent implements EventBus.IEvent {
}
